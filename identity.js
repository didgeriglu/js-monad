const MONAD = require ('./monad.js')

var identity = MONAD();
var m = identity("Hello world!");
m.bind(console.log);
